﻿//Created by Rhese Soemo for CST-117, Milestone 2. Submitted for Professor Darwiche//
using System;

namespace InventoryItem_Prototype
{
    class Program
    {
        //================================//
        //=======Main=Driver=Method=======//
        //================================//
        static void Main(string[] args)
        {
            //===Testing=of=InventoryItem===//
            //=Instantiating=//
            InventoryItem headset = new InventoryItem("Headset", 20.50, 15, 10.0, 4.2, "Standard headset");
            InventoryItem keyboard = new InventoryItem("Keyboard", 15.99, 9, 24.5, 9.8, "Standard keyboard");

            //=Printing=Out//
            Console.WriteLine("-> " + headset.getItemName() + " is the name of the item. It sells for " + headset.getItemPrice() + ", there are " + 
                headset.getItemStock() + " in stock, weighing " + headset.getItemWeight() + ", has the size of " + headset.getItemSize() + ", & has the description of '" 
                + headset.getItemDescription() + "'");
            Console.WriteLine();
            Console.WriteLine("-> " + keyboard.getItemName() + " is the name of the item. It sells for " + keyboard.getItemPrice() + ", there are " +
                keyboard.getItemStock() + " in stock, weighing " + keyboard.getItemWeight() + ", has the size of " + keyboard.getItemSize() + ", & has the description of '"
                + keyboard.getItemDescription() + "'");

            //=Changing=Values=//
            headset.setItemName("Headset Pro");
            headset.setItemPrice(49.8);
            headset.setItemStock(30);
            headset.setItemWeight(11.5);
            headset.setItemSize(5.2);
            headset.setItemDescription("Pro headset");

            //=Printing=Out//
            Console.WriteLine("");
            Console.WriteLine("-> Using setters on headset and printing out:");
            Console.WriteLine("-> " + headset.getItemName() + " is the name of the item. It sells for " + headset.getItemPrice() + ", there are " +
                headset.getItemStock() + " in stock, weighing " + headset.getItemWeight() + ", has the size of " + headset.getItemSize() + ", & has the description of '"
                + headset.getItemDescription() + "'");
            Console.WriteLine();
        }
    }
}
