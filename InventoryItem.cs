﻿//Created by Rhese Soemo for CST-117, Milestone 2. Submitted for Professor Darwiche//
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryItem_Prototype
{
    public class InventoryItem
{
	//===============================//
	//=======Private=Variables=======//
	//===============================//
	private String itemName;
	private double itemPrice;
	private int itemStock;
	private double itemWeight;
	private double itemSize;
	private String itemDescription;

	//=========================//
	//=======Constructor=======//
	//=========================//
	public InventoryItem(String itemName, double itemPrice, int itemStock, double itemWeight, double itemSize, String itemDescription)
	{
		this.itemName = itemName;
		this.itemPrice = itemPrice;
		this.itemStock = itemStock;
		this.itemWeight = itemWeight;
		this.itemSize = itemSize;
		this.itemDescription = itemDescription;
	}

	//=====================//
	//=======Getters=======//
	//=====================//
	//===getItemName===//
	public String getItemName()
	{
		return itemName;
	}
	//===getItemPrice===//
	public double getItemPrice()
	{
		return itemPrice;
	}
	//===getItemStock===//
	public int getItemStock()
	{
		return itemStock;
	}
	//===getItemWeight===//
	public double getItemWeight()
	{
		return itemWeight;
	}
	//===getItemSize===//
	public double getItemSize()
	{
		return itemSize;
	}
	//===getItemDescription===//
	public String getItemDescription()
	{
		return itemDescription;
	}

	//=====================//
	//=======Setters=======//
	//=====================//
	//===setItemName===//
	public void setItemName(String itemName)
	{
		this.itemName = itemName;
	}
	//===setItemPrice===//
	public void setItemPrice(double itemPrice)
	{
		this.itemPrice = itemPrice;
	}
	//===setItemStock===//
	public void setItemStock(int itemStock)
	{
		this.itemStock = itemStock;
	}
	//===setItemWeight===//
	public void setItemWeight(double itemWeight)
	{
		this.itemWeight = itemWeight;
	}
	//===setItemSize===//
	public void setItemSize(double itemSize)
	{
		this.itemSize = itemSize;
	}
	//===setItemDescription===//
	public void setItemDescription(String itemDescription)
	{
		this.itemDescription = itemDescription;
	}

	//===========================//
	//=======Stock=Changes=======//
	//===========================//
	//===addItemStock===//
	public void addItemStock(int itemsToAdd)
	{
		itemStock += itemsToAdd;
	}
	//===addItemStock===//
	public void removeItemStock(int itemsToRemove)
	{
		itemStock -= itemsToRemove;
	}
}
}
